# Matomo Noscript

Some sites have a strict privacy policy which prohibits tracking of browser
metadata tracked by the Matomo JavaScript client (piwik.js).

In addition, some sites have a significant user base using noscript and other
browser extensions which limit ability to execute JavaScript.

This module uses an alternative syntax to setup Matomo tracking code via an
image tag rather than loading piwik.js.

This module adds a noscript tag containing the Matomo image tag to the bottom of
every page.

In addition, if [Matomo module](https://www.drupal.org/project/matomo) is not
enabled, this module also adds some JavaScript to track the referrer URL.

To use this module, you'll need to configure your Matomo settings by either
installing and configuring [Matomo
module](https://www.drupal.org/project/matomo), or by adding these lines to your
settings.php file:

    $config['matomo.settings']['site_id'] = 1;
    $config['matomo.settings']['url_https'] = 'https://matomo.example.org/';

For more info visit the [project
page](https://www.drupal.org/project/piwik_noscript).

Submit bug reports and feature suggestions to the [issue
queue](https://www.drupal.org/project/issues/piwik_noscript).

Maintained by [mfb](https://www.drupal.org/u/mfb).
