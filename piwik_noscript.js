/**
 * @file
 * Add referrer URL tracking to Matomo tracking code.
 */

((drupalSettings) =>
  document.addEventListener(
    'DOMContentLoaded',
    () =>
      drupalSettings.piwikNoscript &&
      navigator.sendBeacon(
        `${drupalSettings.piwikNoscript.url}&urlref=${encodeURIComponent(
          document.referrer,
        )}`,
      ),
  ))(drupalSettings);
