<?php

namespace Drupal\piwik_noscript;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides callbacks for lazy builders.
 */
class TrustedCallback implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['getImage', 'getNoscript'];
  }

  /**
   * Callback for returning an image element.
   *
   * @return mixed[]
   *   Renderable array.
   */
  public static function getImage(): array {
    return piwik_noscript_image(piwik_noscript_options());
  }

  /**
   * Callback for returning the noscript element.
   *
   * @return mixed[]
   *   Renderable array.
   */
  public static function getNoscript(): array {
    return [
      [
        '#type' => 'html_tag',
        '#tag' => 'noscript',
        'child' => static::getImage(),
        '#attributes' => ['class' => ['piwik-noscript']],
      ],
    ];
  }

}
