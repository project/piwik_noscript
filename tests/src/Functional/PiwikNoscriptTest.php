<?php

namespace Drupal\Tests\piwik_noscript\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Matomo Noscript module.
 *
 * @group piwik_noscript
 */
class PiwikNoscriptTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['piwik_noscript'];

  /**
   * Tests Matomo Noscript module.
   */
  public function testPiwikNoscript(): void {
    $settings['config']['matomo.settings'] = [
      'site_id' => (object) ['value' => 1, 'required' => TRUE],
      'url_https' => (object) ['value' => 'https://example.test/', 'required' => TRUE],
    ];
    $this->writeSettings($settings);
    $user = $this->drupalCreateUser([]);
    $this->assertNotEmpty($user);
    $this->drupalLogin($user);
    $xpath = $this->assertSession()->buildXPathQuery('//noscript[@class="piwik-noscript"]/img[@loading="eager"]', []);
    $this->assertSession()->elementsCount('xpath', $xpath, 1);
  }

}
